#include "threadobjects/Manager.hpp"

#include "global.h"

#include <iostream>
#include <fstream>

#include <array>
#include <algorithm>

#include <boost/filesystem.hpp>

#include "Contact.hpp"

#include "events/GetEvent.hpp"
#include "events/PutEvent.hpp"

using namespace pinfs::threadobjects;

void Manager::addToQueue(events::Event* event) {
  queue_.push(EventPtr(event));
}

void Manager::init() {
  using namespace std::placeholders;

  std::cout << "INIT\nMy key: " << rt_.getContactToMe().id
            << std::endl;

  expiryDeadline_.expires_from_now(boost::posix_time::minutes(5));
  expiryDeadline_.async_wait(std::bind(&Manager::checkExpiredRequests,
                                       std::ref(*this), _1));

  pingAllDeadline_.expires_from_now(boost::posix_time::minutes(2));
  pingAllDeadline_.async_wait(std::bind(&Manager::pingAll,
                                        std::ref(*this), _1));

  //perform bootstrap
  if(bootstrap_.given == Bootstrap::YES) {
    std::cout << "BOOTSTRAPING TO ADDRESS " << std::hex
              << bootstrap_.bootstrapAddress.ip_address << std::dec << ":"
              << bootstrap_.bootstrapAddress.port << std::endl;

    int requestId = nextMsgId_++;

    messages::FindNodeRequest* ptr = new messages::FindNodeRequest(requestId, rt_.getContactToMe());
    unhandled_.emplace(requestId, RequestInfo(messages::Message::FNODE_REQ, pinfs::Key::GenerateRandom(), ioc_));

    ptr->searched = rt_.getContactToMe().id;

    ms_.addToQueue(ptr, bootstrap_.bootstrapAddress);
  }

  //bootstrap from known node if given

  //BELOW = test code, remove this before release!!!!!!!
  // Contact myself(0x7F000001, 50001, rt_.getKey());
  // rt_.update(myself);

  // messages::PingRequest* pingreq = new messages::PingRequest(100, myself);
  // addToQueue(pingreq);

  // boost::filesystem::path path("testfiles/file1.txt");
  // messages::StoreRequest* storereq = new messages::StoreRequest(0, rt_.getContactToMe(), path.filename().string());

  // std::ifstream fin(path.string(), std::ios::in | std::ios::binary);
  // std::array<unsigned char, 512> buf;

  // while(fin.read(reinterpret_cast<char*>(buf.data()), 512).gcount() > 0) {
  //   int nchars = fin.gcount();
  //   storereq->filecontent.insert(storereq->filecontent.begin(), buf.begin(), buf.begin() + nchars);
  // }

  // std::cout << "FILE ADDED" << std::endl;

  // addToQueue(storereq);
}

void Manager::checkExpiredRequests(const boost::system::error_code& ec) {
  std::cout << "CHECKING FOR EXPIRED UNHANDLED REQUESTS" << std::endl;

  using namespace std::placeholders;

  for(auto it=unhandled_.begin(); it!=unhandled_.end();) {
    if(it->second.expiryTime.expires_at() < boost::asio::deadline_timer::traits_type::now()) {
      rt_.removeKey(it->second.receiverId);

      if(it->first == session_.lastRequestId) {
        //TODO close session and indicate failure
        session_.reset();
        cs_.synchronize("Session expired");
      }

      it = unhandled_.erase(it);
    }
    else
      ++it;
  }

  expiryDeadline_.expires_from_now(boost::posix_time::minutes(5));
  expiryDeadline_.async_wait(std::bind(&Manager::checkExpiredRequests,
                                       std::ref(*this), _1));
}

void Manager::pingAll(const boost::system::error_code& ec) {
  std::cout << "RUNNING PING ALL" << std::endl;

  using namespace std::placeholders;

  const RoutingTable::BucketArray& buckets = rt_.getAllBuckets();

  for(auto& bucket : buckets) {
    for(auto& contact : bucket) {
      int reqId = nextMsgId_++;

      unhandled_.emplace(reqId, RequestInfo(messages::Message::PING_REQ, contact.id, ioc_));
      messages::PingRequest* ping = new messages::PingRequest(reqId, rt_.getContactToMe());

      ms_.addToQueue(ping, contact.address);
    }
  }

  pingAllDeadline_.expires_from_now(boost::posix_time::minutes(2));
  pingAllDeadline_.async_wait(std::bind(&Manager::pingAll,
                                        std::ref(*this), _1));
}

void Manager::handleStoreRequest(const messages::StoreRequest& storereq) {
  std::cout << "STORE REQUEST from " << storereq.contact.id << std::endl;

  boost::filesystem::path path(storereq.filename);
  std::string filename(path.filename().string());

  if(!knownFileIds_.count(filename)) {
    //create and write file
    std::ofstream fout(storePath_ + filename, std::ios::out | std::ios::binary);
    fout.write(reinterpret_cast<const char*>(storereq.filecontent.data()), storereq.filecontent.size());

    knownFileIds_.emplace(filename, pinfs::Key::FromString(filename));
  }

  messages::StoreResponse* res = new messages::StoreResponse(storereq.id, rt_.getContactToMe());
  ms_.addToQueue(res, storereq.contact.address);
}

void Manager::handleFNodeRequest(const messages::FindNodeRequest& fnodereq) {
  std::cout << "FNODE REQUEST from " << fnodereq.contact.id << std::endl;

  auto& nearestContacts = rt_.getBucket(fnodereq.searched);

  messages::FindNodeResponse* res = new messages::FindNodeResponse(fnodereq.id, rt_.getContactToMe());

  res->nearest = rt_.findKClosest(fnodereq.searched);

  ms_.addToQueue(res, fnodereq.contact.address);
}

void Manager::handleFValueRequest(const messages::FindValueRequest& fvaluereq) {
  std::cout << "FVALUE REQUEST from " << fvaluereq.contact.id << std::endl;

  auto it = std::find_if(knownFileIds_.begin(),
                         knownFileIds_.end(),
                         [&fvaluereq](const IdList::value_type& fileassoc) {
                           return fvaluereq.searched == fileassoc.second;
                         });

  messages::FindValueResponse* res = new messages::FindValueResponse(fvaluereq.id, rt_.getContactToMe());

  if(it != knownFileIds_.end()) {
    //send back file
    res->rtype = messages::FindValueResponse::FILE;

    std::ifstream fin(storePath_ + it->first, std::ios::in | std::ios::binary);
    std::array<unsigned char, 512> buf;

    int nchars;
    while( (nchars = fin.read(reinterpret_cast<char*>(buf.data()), 512).gcount()) > 0 ) {
      res->filecontent.insert(res->filecontent.end(), buf.begin(), buf.begin() + nchars);
    }
  }
  else {
    //send the same as FindNodeResponse
    res->rtype = messages::FindValueResponse::NOFILE;

    res->nearest = rt_.findKClosest(fvaluereq.searched);
  }

  ms_.addToQueue(res, fvaluereq.contact.address);
}

void Manager::handleExistsRequest(const messages::ExistsRequest& existsreq) {
  auto it = std::find_if(knownFileIds_.begin(),
                         knownFileIds_.end(),
                         [&existsreq](const IdList::value_type& fileassoc) {
                           return existsreq.fileId == fileassoc.second;
                         });

  messages::ExistsResponse* res = new messages::ExistsResponse(existsreq.id, rt_.getContactToMe());
  res->fileExists = (it != knownFileIds_.end());

  ms_.addToQueue(res, existsreq.contact.address);
}

void Manager::handleStoreResponse(const messages::StoreResponse& storeres) {
  std::cout << "STORE RESPONSE from " << storeres.contact.id << std::endl;

  if(session_.isActive() && session_.type == CommandSession::PUT && session_.lastRequestId == storeres.id) {
    // close session (success)

    session_.reset();
    cs_.synchronize("Put successful");
  }
}

void Manager::handleFNodeResponse(const messages::FindNodeResponse& fnoderes) {
  std::cout << "FNODE RESPONSE from " << fnoderes.contact.id << " ID: " << fnoderes.id << std::endl;

  if(session_.isActive()) {
    std::cout << "Last session request ID: " << session_.lastRequestId << std::endl;
  }

  if(session_.isActive() && session_.type == CommandSession::PUT && session_.lastRequestId == fnoderes.id) {
    // update session info, store if condition fulfilled

    bool incrementCounter = true;

    for(auto& c : fnoderes.nearest) {
      rt_.update(c);

      Key distance = c.id ^ session_.searched;
      if(distance < session_.nearestDistance) {
        session_.nearestDistance = distance;
        session_.nearestNode = c.address;
        session_.requestsSinceLastImprovement = 0;

        incrementCounter = false;
      }
    }

    if(incrementCounter)
      ++session_.requestsSinceLastImprovement;

    if(session_.requestsSinceLastImprovement > PINFS_MAX_UNIMPROVED_REQUESTS) {
      //issue store request to nearest found node
      int requestId = nextMsgId_++;

      boost::filesystem::path path(session_.filepath);

      messages::StoreRequest* req = new messages::StoreRequest(requestId, rt_.getContactToMe(), path.filename().string());
      session_.lastRequestId = requestId;

      std::array<unsigned char, 512> buf;
      std::ifstream fin(session_.filepath, std::ios::in | std::ios::binary);

      int nchars;
      while( (nchars = fin.read(reinterpret_cast<char*>(buf.data()), 512).gcount()) > 0 ) {
        req->filecontent.insert(req->filecontent.end(), buf.begin(), buf.begin() + nchars);
      }

      // req->filename = path.filename().string();

      std::cout << "SENDING STORE REQUEST with file " << req->filename << std::endl;

      unhandled_.emplace(requestId, RequestInfo(messages::Message::STORE_REQ, pinfs::Key::GenerateRandom(), ioc_));

      ms_.addToQueue(req, session_.nearestNode);
    }
    else {
      //issue find node request to nearest found node
      int requestId = nextMsgId_++;

      messages::FindNodeRequest* req = new messages::FindNodeRequest(requestId, rt_.getContactToMe());
      req->searched = session_.searched;

      session_.lastRequestId = requestId;

      std::cout << "SENDING FNODE REQUEST ID: " << requestId << ", address: "
                << std::hex << session_.nearestNode.ip_address << std::dec << std::endl;

      unhandled_.emplace(requestId, RequestInfo(messages::Message::FNODE_REQ, pinfs::Key::GenerateRandom(), ioc_));

      ms_.addToQueue(req, session_.nearestNode);
    }
  }
  else {
    for(auto& c : fnoderes.nearest)
      rt_.update(c);
  }
}

void Manager::handleFValueResponse(const messages::FindValueResponse& fvalueres) {
  std::cout << "FVALUE RESPONSE from " << fvalueres.contact.id << std::endl;

  if(session_.isActive() && session_.type == CommandSession::GET && session_.lastRequestId == fvalueres.id) {
    // update session info, close session if file delivered
    switch(fvalueres.rtype) {
    case messages::FindValueResponse::NOFILE:
      {
        //perform another step in node lookup
        bool incrementCounter = true;

        for(auto& c : fvalueres.nearest) {
          rt_.update(c);

          Key distance = c.id ^ session_.searched;
          if(distance < session_.nearestDistance) {
            session_.nearestDistance = distance;
            session_.nearestNode = c.address;
            session_.requestsSinceLastImprovement = 0;

            incrementCounter = false;
          }
        }

        if(incrementCounter) {
          ++session_.requestsSinceLastImprovement;
        }

        if(session_.requestsSinceLastImprovement > PINFS_MAX_UNIMPROVED_REQUESTS) {
          //abandon session with failure as result

          session_.reset();
          cs_.synchronize("Get failed");
        }
        else {
          //issue find value request to nearest found node
          int requestId = nextMsgId_++;

          messages::FindValueRequest* req = new messages::FindValueRequest(requestId, rt_.getContactToMe());
          req->searched = session_.searched;

          session_.lastRequestId = requestId;

          unhandled_.emplace(requestId, RequestInfo(messages::Message::FVALUE_REQ, pinfs::Key::GenerateRandom(), ioc_));

          std::cout << "SENDING FVALUE REQUEST" << std::endl;

          ms_.addToQueue(req, session_.nearestNode);
        }
      }
      break;

    case messages::FindValueResponse::FILE:
      //put file in storage and close session (success)
      boost::filesystem::path path(session_.filepath);
      std::ofstream fout(storePath_ + path.filename().string(), std::ios::out | std::ios::binary);
      fout.write(reinterpret_cast<const char*>(fvalueres.filecontent.data()), fvalueres.filecontent.size());

      session_.reset();
      cs_.synchronize("Get successful");
      break;
    }
  }
}

void Manager::handleRequest(const messages::Message* msg) {
  rt_.update(msg->contact);
}

void Manager::handleResponse(const messages::Message* msg) {
  if(unhandled_.count(msg->id)) {
    rt_.update(msg->contact);
    unhandled_.erase(msg->id);
  }
}

void Manager::eventLoop(const boost::system::error_code& ec) {
    // std::cout << "LOOP" << std::endl;

    EventPtr event = queue_.popOrElse(EventPtr(new events::Event));

    // std::cout << "EVENT EXTRACTED" << std::endl;

    messages::Message* msgptr;

    switch(event->type) {
    case events::Event::MSG:
      {
        messages::Message* msgptr = dynamic_cast<messages::Message*>(event.get());
        switch(msgptr->type) {
        case messages::Message::PING_REQ:
          {
            messages::PingRequest* ptr = dynamic_cast<messages::PingRequest*>(msgptr);
            handleRequest(msgptr);

            std::cout << "PING REQUEST from " << msgptr->contact.id << std::endl;

            messages::PingResponse* pingres = new messages::PingResponse(ptr->id, rt_.getContactToMe());
            ms_.addToQueue(pingres, ptr->contact.address);
          }
          break;

        case messages::Message::PING_RES:
          {
            handleResponse(msgptr);

            std::cout << "PING RESPONSE from " << msgptr->contact.id << std::endl;
          }
          break;

        case messages::Message::STORE_REQ:
          {
            messages::StoreRequest* ptr = dynamic_cast<messages::StoreRequest*>(msgptr);
            handleRequest(msgptr);

            handleStoreRequest(*ptr);
          }
          break;

        case messages::Message::STORE_RES:
          {
            messages::StoreResponse* ptr = dynamic_cast<messages::StoreResponse*>(msgptr);
            handleStoreResponse(*ptr);

            handleResponse(msgptr);
          }
          break;

        case messages::Message::FNODE_REQ:
          {
            messages::FindNodeRequest* ptr = dynamic_cast<messages::FindNodeRequest*>(msgptr);
            handleRequest(msgptr);

            handleFNodeRequest(*ptr);
          }
          break;

        case messages::Message::FNODE_RES:
          {
            messages::FindNodeResponse* ptr = dynamic_cast<messages::FindNodeResponse*>(msgptr);
            handleFNodeResponse(*ptr);

            handleResponse(msgptr);
          }
          break;

        case messages::Message::FVALUE_REQ:
          {
            messages::FindValueRequest* ptr = dynamic_cast<messages::FindValueRequest*>(msgptr);
            handleRequest(msgptr);

            handleFValueRequest(*ptr);
          }
          break;

        case messages::Message::FVALUE_RES:
          {
            messages::FindValueResponse* ptr = dynamic_cast<messages::FindValueResponse*>(msgptr);
            handleFValueResponse(*ptr);

            handleResponse(msgptr);
          }
          break;

        case messages::Message::EXISTS_REQ:
          {
            messages::ExistsRequest* ptr = dynamic_cast<messages::ExistsRequest*>(msgptr);
            handleRequest(msgptr);

            handleExistsRequest(*ptr);
          }
          break;

        case messages::Message::EXISTS_RES:
          {
            // confirm response?
            handleResponse(msgptr);
          }
          break;
        }
      }
      break;

    case events::Event::PUT:
      {
        if(session_.isActive()) {
          //TODO error
        }

        events::PutEvent* evt = dynamic_cast<events::PutEvent*>(event.get());

        std::cout << "PUT " << evt->fileName << std::endl;

        int requestId = nextMsgId_++;

        messages::FindNodeRequest* req = new messages::FindNodeRequest(requestId, rt_.getContactToMe());
        boost::filesystem::path path(evt->fileName);

        session_.type = CommandSession::PUT;
        session_.searched = pinfs::Key::FromString(path.filename().string());
        session_.lastRequestId = requestId;
        session_.filepath = path.string();

        req->searched = session_.searched;

        auto nearest = rt_.findKClosest(session_.searched);

        if(nearest.empty()) {
          std::cout << "No nearest nodes found!" << std::endl;
          delete req;

          session_.reset();
          cs_.synchronize("Put failed - empty routing table");
        }
        else {
          unhandled_.emplace(requestId, RequestInfo(messages::Message::FNODE_REQ, nearest.front().id, ioc_));

          std::cout << "SENDING FNODE REQUEST ID: " << requestId << " to " << nearest.front().id << std::endl;

          ms_.addToQueue(req, nearest.front().address);
        }
      }
      break;

    case events::Event::GET:
      {
        if(session_.isActive()) {
          //TODO error
        }
        events::GetEvent* evt = dynamic_cast<events::GetEvent*>(event.get());

        std::cout << "GET " << evt->fileName << std::endl;

        int requestId = nextMsgId_++;

        messages::FindValueRequest* req = new messages::FindValueRequest(requestId, rt_.getContactToMe());
        boost::filesystem::path path(evt->fileName);

        session_.type = CommandSession::GET;
        session_.filepath = path.string();
        session_.searched = pinfs::Key::FromString(path.filename().string());
        session_.lastRequestId = requestId;

        req->searched = session_.searched;

        auto nearest = rt_.findKClosest(session_.searched);

        if(nearest.empty()) {
          std::cout << "No nearest nodes found!" << std::endl;
          delete req;

          session_.reset();
          cs_.synchronize("Get failed - empty routing table");
        }
        else {
          unhandled_.emplace(requestId, RequestInfo(messages::Message::FVALUE_REQ, nearest.front().id, ioc_));

          std::cout << "SENDING FNODE REQUEST to " << nearest.front().id << std::endl;

          ms_.addToQueue(req, nearest.front().address);
        }
      }
      break;

    case events::Event::NONE:
      // std::cout << "NO EVENT IN QUEUE" << std::endl;
      break;
    }
}

void Manager::executor() {
  using namespace std::placeholders;

  boost::asio::deadline_timer eventDeadline(ioc_);
  eventDeadline.expires_from_now(boost::posix_time::seconds(1));
  eventDeadline.async_wait(std::bind(&Manager::eventLoop, std::ref(*this), _1));

  init();

  while(1) {
    // boost::asio::post(ioc_, std::bind(&Manager::eventLoop, std::ref(*this)));
    if(eventDeadline.expires_at() < boost::asio::deadline_timer::traits_type::now()) {
      eventDeadline.expires_from_now(boost::posix_time::milliseconds(10));
      eventDeadline.async_wait(std::bind(&Manager::eventLoop, std::ref(*this), _1));
    }

    ioc_.run_one();
    ioc_.restart();
  }
}
