#include "threadobjects/MasterReceiver.hpp"

#include "messages/MessageDeserializer.hpp"

#include "boost/asio.hpp"
#include <thread>

using namespace boost::asio;
using namespace pinfs::threadobjects;

void appendBuffer(const char* buf, int n, std::string& dest) {
  dest.append(buf, n);
}

void MasterReceiver::executor() {
  while(1) {
    std::string msgstr = queue_.pop();
    messages::Message* msg = messages::MessageDeserializer::Deserialize(msgstr);

    // std::cout << msg->id << std::endl;

    manager_.addToQueue(dynamic_cast<events::Event*>(msg));
  }
}

void pinfs::threadobjects::receiverWorker(MasterReceiver& mr, io_context& ioc, unsigned short port) {
  ip::tcp::endpoint ep(ip::tcp::v4(), port);
  ip::tcp::acceptor acceptor(ioc, ep);
  boost::system::error_code ec;
  char recvbuffer[512];
  std::string resultmsg;

  //work forever
  while(1) {

    while(1) {
      ip::tcp::socket socket(ioc);
      acceptor.accept(socket);

      int nbytes =
        boost::asio::read(socket, boost::asio::buffer(recvbuffer, sizeof(recvbuffer)), ec);

      appendBuffer(recvbuffer, nbytes, resultmsg);

      if(ec == error::eof) {
        // add msg to MasterReceiver's queue, clear result
        mr.addToQueue(resultmsg);
        resultmsg.clear();
        break;
      }
      else if(ec) {
        resultmsg.clear();
        break;
      }
    }//while inner

  }//while outer
}

void MasterReceiver::addToQueue(const std::string& encodedmsg) {
  queue_.push(encodedmsg);
}
