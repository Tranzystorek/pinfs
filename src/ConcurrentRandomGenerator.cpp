#include "concurrent/ConcurrentRandomGenerator.hpp"

#include <chrono>

using namespace pinfs::concurrent;

ConcurrentRandomGenerator::ConcurrentRandomGenerator()
  : engine_(std::chrono::system_clock::now()
            .time_since_epoch()
            .count()) {
}

ConcurrentRandomGenerator& ConcurrentRandomGenerator::Instance() {
  static ConcurrentRandomGenerator instance;

  return instance;
}

ConcurrentRandomGenerator::Result ConcurrentRandomGenerator::generate() {
  mutex_.lock();
  Result ret = engine_();
  mutex_.unlock();

  return ret;
}

