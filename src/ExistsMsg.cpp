#include "messages/ExistsMsg.hpp"

#include "global.h"
#include "utility.hpp"

using namespace pinfs::messages;

std::string ExistsRequest::serialize() {
  std::string ret;
  const int size = msgHeaderSize()
    + sizeof(pinfs::Key);

  ret += makeHeader(size);
  ret.append(reinterpret_cast<const char*>(fileId.bytes.data()), PINFS_KEY_LENGTH);

  return ret;
}

std::string ExistsResponse::serialize() {
  std::string ret;
  const int size = msgHeaderSize();
    + PINFS_CHAR_FIELD_WIDTH;

  ret += makeHeader(size);
  ret += ((char)fileExists);

  return ret;
}
