#include "Key.hpp"

#include "hashlib++/hashlibpp.h"
#include "hashlib++/hl_sha1wrapper.h"

#include "concurrent/ConcurrentRandomGenerator.hpp"

#include "utility.hpp"

#include <cstring>
#include <algorithm>

using namespace pinfs;

Key::Key(const char* str) {
  std::memcpy(bytes.data(), str,
              PINFS_KEY_LENGTH);
}

Key::Key(const Key& other) {
  std::memcpy(bytes.data(),
              other.bytes.data(),
              PINFS_KEY_LENGTH);
}

Key& Key::operator=(const char* str) {
  std::memcpy(bytes.data(), str,
              PINFS_KEY_LENGTH);

  return *this;
}

Key& Key::operator=(const Key& other) {
  std::memcpy(bytes.data(),
              other.bytes.data(),
              PINFS_KEY_LENGTH);

  return *this;
}

bool pinfs::Key::operator==(const Key& other) const {
  for(int i=0; i<PINFS_KEY_LENGTH; ++i) {
    if(bytes[i] != other.bytes[i])
      return false;
  }

  return true;
}

bool Key::operator!=(const Key& other) const {
  for(int i=0; i<PINFS_KEY_LENGTH; ++i) {
    if(bytes[i] != other.bytes[i]) {
      return true;
    }
  }

  return false;
}

bool pinfs::Key::operator<(const Key& other) const {
  for(int i=0; i<PINFS_KEY_LENGTH; ++i) {
    if(bytes[i] != other.bytes[i])
      return bytes[i] < other.bytes[i];
  }

  return false;
}

Key Key::operator^(const Key& other) const {
  Key ret;

  for(int i=0; i<PINFS_KEY_LENGTH; ++i) {
    ret.bytes[i] = bytes[i] ^ other.bytes[i];
  }

  return ret;
}

int Key::getPrefixLength() const {
  for(int i=0; i<PINFS_KEY_LENGTH; ++i) {
    for(int j=7; j>=0; --j) {
      if( (bytes[i] >> j) & 0x1 )
        return i * 8 + (7 - j);
    }
  }

  return PINFS_KEY_LENGTH * 8 - 1;
}

std::string Key::toHexString() const {
  return utility::bytesToHexString(bytes.data(), bytes.size());
}

Key Key::FromByteChunk(const std::string& str, int pos) {
  Key ret;

  if(str.size() < pos + PINFS_KEY_LENGTH) {
    //TODO error
  }

  const int end = pos + PINFS_KEY_LENGTH;

  for(int i=pos; i<end; ++i) {
    ret.bytes[i-pos] = str[i];
  }

  return ret;
}

Key Key::FromHexString(const std::string& hexstr) {
  Key ret;

  if(hexstr.size() != PINFS_KEY_LENGTH * 2) {
    //TODO error
  }

  for(int i=0; i<PINFS_KEY_LENGTH*2; i+=2) {
    ret.bytes[i/2] = (utility::fromHex(hexstr[i]) << 4) | (utility::fromHex(hexstr[i+1]) & 0xF);
  }

  return ret;
}

Key pinfs::Key::FromString(const std::string& unhashed) {
  sha1wrapper wrapper;

  return FromHexString(wrapper.getHashFromString(unhashed));
}

Key Key::GenerateRandom() {
  Key ret;

  for(auto& b : ret.bytes) {
    b = concurrent::ConcurrentRandomGenerator::Instance().generate();
  }

  return ret;
}

std::ostream& pinfs::operator<<(std::ostream& os, const Key& k) {
  os << k.toHexString();
  return os;
}

