#ifndef PINFS_MESSAGE_DESERIALIZER_HPP
#define PINFS_MESSAGE_DESERIALIZER_HPP

#include "messages/Message.hpp"

#include "messages/PingMsg.hpp"
#include "messages/StoreMsg.hpp"
#include "messages/FindNodeMsg.hpp"
#include "messages/FindValueMsg.hpp"
#include "messages/ExistsMsg.hpp"

namespace pinfs {
namespace messages {

class MessageDeserializer {
public:
  static Message* Deserialize(const std::string& msgstring);
};

} //messages
} //pinfs

#endif //PINFS_MESSAGE_DESERIALIZER_HPP
