#ifndef PINFS_MESSAGE_HPP
#define PINFS_MESSAGE_HPP

#include "events/Event.hpp"

#include "Contact.hpp"
#include "global.h"
#include "utility.hpp"

#include <unordered_map>
#include <string>

namespace pinfs {
namespace messages {

constexpr int msgHeaderSize() {
  return 2 * PINFS_INT_FIELD_WIDTH + PINFS_CHAR_FIELD_WIDTH
    + PINFS_LONG_FIELD_WIDTH + PINFS_SHORT_FIELD_WIDTH
    + sizeof(pinfs::Key);
}

struct Message : public events::Event {
  //using Properties = std::unordered_map<unsigned char, std::string>;

  enum Type : unsigned char {
    PING_REQ, PING_RES,
    STORE_REQ, STORE_RES,
    FNODE_REQ, FNODE_RES,
    FVALUE_REQ, FVALUE_RES,
    EXISTS_REQ, EXISTS_RES,
    INVALID
  } type;

  int id;
  Contact contact;

  Message(Type t, int id = 0, const Contact& contact = Contact())
    : events::Event(events::Event::MSG),
      type(t),
      id(id),
      contact(contact) {
  }

  virtual ~Message() {
  }

  virtual std::string serialize() = 0;

  std::string makeHeader(int size) {
    std::string result;
    //const int size = 2 * sizeof(int) + sizeof(char)
    //  + sizeof(pinfs::Address) + sizeof(pinfs::Key) + PINFS_NULL_TERMINATOR_SIZE;

    result.append(reinterpret_cast<const char*>(utility::ToBytes<int, PINFS_INT_FIELD_WIDTH>(size).data()),
                  PINFS_INT_FIELD_WIDTH);
    result.append(reinterpret_cast<const char*>(utility::ToBytes<int, PINFS_INT_FIELD_WIDTH>(id).data()),
                  PINFS_INT_FIELD_WIDTH);
    result += (char)(type);
    result.append(reinterpret_cast<const char*>(utility::ToBytes<unsigned long, PINFS_LONG_FIELD_WIDTH>(contact.address.ip_address).data()),
                  PINFS_LONG_FIELD_WIDTH);
    result.append(reinterpret_cast<const char*>(utility::ToBytes<unsigned short, PINFS_SHORT_FIELD_WIDTH>(contact.address.port).data()),
                  PINFS_SHORT_FIELD_WIDTH);
    result.append(reinterpret_cast<const char*>(contact.id.bytes.data()),
                  sizeof(pinfs::Key));

    return result;
  }
};

} //messages
} //pinfs

#endif //PINFS_MESSAGE_HPP
