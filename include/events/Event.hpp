#ifndef PINFS_EVENT_HPP
#define PINFS_EVENT_HPP

namespace pinfs {
namespace events {

struct Event {
  enum Type {
     NONE,
     MSG,
     PUT,
     GET
  } type;

  virtual ~Event() {
  }

  Event(Type t = Type::NONE)
    : type(t) {
  }
};

} //events
} //pinfs

#endif //PINFS_EVENT_HPP
