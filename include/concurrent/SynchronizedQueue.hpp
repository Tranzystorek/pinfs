#ifndef PINFS_SYNCHRONIZED_QUEUE_HPP
#define PINFS_SYNCHRONIZED_QUEUE_HPP

#include <queue>
#include <string>
#include <mutex>
#include <condition_variable>

namespace pinfs {
namespace concurrent {

template<class T>
class SynchronizedQueue {
public:
  using StoredType = T;
  using ConstReference = const StoredType&;
public:
  void push(ConstReference el) {
    std::unique_lock<std::mutex> lock(mutex_);

    queue_.push(el);

    if(queue_.size() == 1)
      cv_.notify_one();
  }

  StoredType pop() {
    //lock mutex and wait on cv if empty
    std::unique_lock<std::mutex> lock(mutex_);
    cv_.wait(lock,
             [this](){
               return !queue_.empty();
             });

    StoredType ret = queue_.front();
    queue_.pop();

    lock.unlock();

    return ret;
  }

  StoredType popOrElse(StoredType defaultValue) {
    StoredType ret;

    std::unique_lock<std::mutex> lock(mutex_);

    if(queue_.empty())
      return defaultValue;

    ret = queue_.front();
    queue_.pop();

    return ret;
  }

private:
  std::queue<StoredType> queue_;
  std::mutex mutex_;
  std::condition_variable cv_;
};

} //concurrent
} //pinfs

#endif //PINFS_SYNCHONIZED_QUEUE_HPP
