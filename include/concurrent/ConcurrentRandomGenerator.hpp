#ifndef PINFS_CONCURRENT_RANDOM_GENERATOR_HPP
#define PINFS_CONCURRENT_RANDOM_GENERATOR_HPP

#include <random>
#include <mutex>

namespace pinfs {
namespace concurrent {

class ConcurrentRandomGenerator {
public:
  using Result = std::default_random_engine::result_type;
private:
  ConcurrentRandomGenerator();

public:
  static ConcurrentRandomGenerator& Instance();

  Result generate();

private:
  std::mutex mutex_;
  std::default_random_engine engine_;
};

} //concurrent
} //pinfs

#endif //PINFS_CONCURRENT_RANDOM_GENERATOR_HPP
