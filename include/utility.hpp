#ifndef PINFS_UTILITY_HPP
#define PINFS_UTILITY_HPP

#include <type_traits>

#include <array>
#include <cstring>

namespace pinfs {
namespace utility {

template<class T, unsigned int N = sizeof(T)>
using Bytes = std::array<unsigned char, N>;

template<class T, unsigned int N = sizeof(T)>
Bytes<T, N> ToBytes(T val) {
  static_assert(std::is_trivial<T>::value, "Bytes requires a trivial type for conversion");

  Bytes<T, N> ret;

  union converter {
    unsigned char bytes[N];
    T converted;
  } c; c.converted = val;

  std::memcpy(ret.data(), c.bytes, N);

  if(N > sizeof(T)) {
    for(int i=sizeof(T); i<N; ++i)
      ret[i] = 0x00;
  }

  return ret;
}

  template<class T, unsigned int N = sizeof(T)>
T FromStringPartial(const std::string& str, int pos) {
  static_assert(std::is_trivial<T>::value, "Conversion requires a trivial type");

  union converter {
    char bytes[N];
    T result;
  } c;

  const int end = pos + N;

  for(int i=pos; i<end; ++i) {
    c.bytes[i-pos] = str[i];
  }

  for(int i=N; i<sizeof(T); ++i) {
    c.bytes[i] = 0x00;
  }

  return c.result;
}

inline std::string bytesToHexString(const unsigned char* bytes, int n) {
  std::string ret;
  char output;

  for(int i=0; i<n; ++i) {
    const unsigned char b = bytes[i];
    //first byte half
    output = b >> 4;
    if(output < 10) output += '0';
    else output += 'a' - 10;
    ret += output;

    //second byte half
    output = b & 0xF;
    if(output < 10) output += '0';
    else output += 'a' - 10;
    ret += output;
  }

  return ret;
}

inline int fromHex(char c) {
  if(c >= '0' && c <= '9')
    return c - '0';

  if(c >= 'a' && c <= 'f')
    return c - ('a' - 10);

  return -1;
}

} //utility
} //pinfs

#endif //PINFS_UTILITY_HPP
